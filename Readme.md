# 单精度软浮点

详见：<https://www.52pojie.cn/thread-1830228-1-1.html>，也可以看[这里](tutorial.md)

请使用gcc或者clang编译。

具体编译命令看sf32test.c文件开头，也可以直接运行buildtest.bat或buildtest_clang.bat。

运行需要cmd或powershell环境。

同样也支持Linux，具体看sf32test.c文件。

请注意，对于舍入，默认的舍入结果与x86-64和AArch64一致，但是截断舍入由于平台之间存在区别，本项目以x86-64为准。

代码基于MIT许可证开源。

## 牛顿迭代近似倒数

详见：<https://www.52pojie.cn/thread-1839972-1-1.html>，也可以看[这里](approxrecip.md)

代码在 [approxrecip](approxrecip) 目录可以找到。

## 一个挺简单的crackme

详见：<https://www.52pojie.cn/thread-1894111-1-1.html>

在 [crackme](crackme) 目录可以找到源码。
