#include "approxrecip.h"

static const uint8_t initreciptable8x8[8] = {
    0xf1, 0xd8, 0xc3, 0xb2, 0xa4, 0x98, 0x8d, 0x84
};

static const uint8_t initreciptable16x8[16] = {
    0xf8, 0xea, 0xdd, 0xd2, 0xc8, 0xbf, 0xb6, 0xae,
    0xa7, 0xa1, 0x9b, 0x95, 0x90, 0x8b, 0x86, 0x82
};

uint32_t getapproxrecip8x8(uint32_t n)
{
    uint32_t x, t;
    x = initreciptable8x8[(n >> 28) & 7] << 24; // 查表估计初值
    // 第一轮迭代
    t = ((uint64_t)x * n) >> 32;
    t = ~t;
    x = ((uint64_t)x * t) >> 31;
    // 第二轮迭代
    t = ((uint64_t)x * n) >> 32;
    t = ~t;
    x = ((uint64_t)x * t) >> 31;
    // 第三轮迭代
    t = ((uint64_t)x * n) >> 32;
    t = ~t;
    x = ((uint64_t)x * t) >> 31;
    return x;
}

uint32_t getapproxrecip8x8_32(uint32_t n)
{
    uint32_t x, t;
    x = initreciptable8x8[(n >> 28) & 7] << 24;
    t = ((uint64_t)x * n) >> 32;
    t = -t;
    x = ((uint64_t)x * t) >> 32;
    x = x << 1;
    t = ((uint64_t)x * n) >> 32;
    t = -t;
    x = ((uint64_t)x * t) >> 32;
    x = x << 1;
    t = ((uint64_t)x * n) >> 32;
    t = ~t;
    x = ((uint64_t)x * t) >> 31;
    return x;
}

uint32_t getapproxrecip16x8(uint32_t n)
{
    uint32_t x, t;
    x = initreciptable16x8[n >> 27 & 15] << 24;
    t = ((uint64_t)x * n) >> 32;
    t = ~t;
    x = ((uint64_t)x * t) >> 31;
    t = ((uint64_t)x * n) >> 32;
    t = ~t;
    x = ((uint64_t)x * t) >> 31;
    t = ((uint64_t)x * n) >> 32;
    t = ~t;
    x = ((uint64_t)x * t) >> 31;
    return x;
}

uint32_t getapproxrecip16x8_32(uint32_t n)
{
    uint32_t x, t;
    x = initreciptable16x8[n >> 27 & 15] << 24;
    t = ((uint64_t)x * n) >> 32;
    t = -t;
    x = ((uint64_t)x * t) >> 32;
    x = x << 1;
    t = ((uint64_t)x * n) >> 32;
    t = -t;
    x = ((uint64_t)x * t) >> 32;
    x = x << 1;
    t = ((uint64_t)x * n) >> 32;
    t = ~t;
    x = ((uint64_t)x * t) >> 31;
    return x;
}

static uint64_t mulhu64(uint64_t u, uint64_t v)
{
    uint64_t w0,w1,w2,u0,u1,v0,v1,t;
    u0 = u & 0xffffffff;
    u1 = u >> 32;
    v0 = v & 0xffffffff;
    v1 = v >> 32;
    w0 = u0 * v0;
    t = u1 * v0 + (w0 >> 32);
    w1 = t & 0xffffffff;
    w2 = t >> 32;
    w1 = u0 * v1 + w1;
    return u1 * v1 + w2 + (w1 >> 32);
}

uint64_t getapproxrecip64_1(uint64_t n)
{
    uint64_t x, t;
    x = (uint64_t)getapproxrecip16x8(n >> 32) << 32;
    t = ~mulhu64(x, n);
    x = mulhu64(x, t) << 1;
    return x;
}

uint64_t getapproxrecip64_2(uint64_t n)
{
    uint64_t x, t;
    x = (uint64_t)getapproxrecip8x8(n >> 32) << 32;
    t = ~mulhu64(x, n);
    x = mulhu64(x, t) << 1;
    t = ~mulhu64(x, n);
    x = mulhu64(x, t) << 1;
    return x;
}
