#pragma once
#include <stdint.h>

uint32_t getapproxrecip8x8(uint32_t n);
uint32_t getapproxrecip8x8_32(uint32_t n);
uint32_t getapproxrecip16x8(uint32_t n);
uint32_t getapproxrecip16x8_32(uint32_t n);

uint64_t getapproxrecip64_1(uint64_t n);
uint64_t getapproxrecip64_2(uint64_t n);
