#include <stdio.h>
#include "approxrecip.c"

static int count_leading_zero(uint32_t x)
{
    if (x == 0)
        return 32;
    int n = 1;
    if ((x >> 16) == 0) { n += 16; x <<= 16; }
    if ((x >> 24) == 0) { n += 8; x <<= 8; }
    if ((x >> 28) == 0) { n += 4; x <<= 4; }
    if ((x >> 30) == 0) { n += 2; x <<= 2; }
    n -= (x >> 31);
    return n;
}

uint32_t udiv_approx(uint32_t a, uint32_t b)
{
    if (a < b)
        return 0;
    int lz = count_leading_zero(b);
    uint32_t recip_b = getapproxrecip8x8(b << lz);
    return ((uint64_t)a * recip_b) >> (63 - lz);
}

uint32_t udiv(uint32_t a, uint32_t b)
{
    if (a < b)
        return 0;
    int lz = count_leading_zero(b);
    uint32_t recip_b = getapproxrecip8x8(b << lz);
    uint32_t quo = ((uint64_t)a * recip_b) >> (63 - lz);
    uint32_t rem = a - quo * b;
    while (rem >= b) {
        quo++;
        rem -= b;
    }
    return quo;
}

void test_udiv(uint32_t a, uint32_t b)
{
    uint32_t q1 = a / b;
    uint32_t q2 = udiv_approx(a, b);
    uint32_t q3 = udiv(a, b);
    printf("%u, %u, %u\n", q1, q2, q3);
}

int main()
{
    test_udiv(32768, 32768);
    test_udiv(0x7fffffff, 0x8001);
    test_udiv(32769, 130);
    test_udiv(12345678, 666);
    test_udiv(1008612306, 52);
    test_udiv(0xffffffff, 5);
    test_udiv(0x8000, 1);
}
