#include <stdio.h>
#include <time.h>
#include "approxrecip.c"

uint32_t lcg_seed;

uint32_t lcg_rand()
{
    lcg_seed = lcg_seed * 134775813 + 1;
    return lcg_seed;
}

uint32_t (*getapproxrecip)(uint32_t);

static uint32_t div_approx(uint32_t a, uint32_t b)
{
    uint32_t recip_b = getapproxrecip(b);
    uint64_t quo = (uint64_t)a * recip_b;
    return quo >> 31;
}

static int check_div_diff(uint32_t a, uint32_t b)
{
    uint32_t q = ((uint64_t)a << 32) / b;
    uint32_t q2 = div_approx(a, b);
    return q2 - q;
}

void random_test_error()
{
    const int LOOP_MAX = 10000000;
    int n[8] = {0};
    for (int i=0; i<LOOP_MAX; i++) {
        int a = (lcg_rand() % 0x80000000 + 0x80000000);
        int b = (lcg_rand() % 0x80000000 + 0x80000000);
        if (a >= b)
            a >>= 1;
        int diff = check_div_diff(a, b);
        diff = -diff;
        n[diff]++;
    }
    for (int i=0; i<8; i++) {
        printf("[-%d]%7.4f%%, ", i, 100.0 * n[i] / LOOP_MAX);
        if ((i+1) % 4 == 0)
            putchar(10);
    }
    uint64_t total_fix = 0;
    for (int i=1; i<8; i++)
        total_fix += i * n[i];
    printf("average fix times = %.3f\n", 1.0 * total_fix / LOOP_MAX);
}

uint32_t div_exact(uint32_t a, uint32_t b)
{
    uint32_t q = div_approx(a, b);
    uint64_t rem = ((uint64_t)a << 32) - ((uint64_t)q * b);
    while (rem >= b) {
        q++;
        rem -= b;
    }
    return q;
}

int main()
{
    lcg_seed = time(NULL);
    getapproxrecip = getapproxrecip8x8;
    printf("8x8:\n");
    random_test_error();
    getapproxrecip = getapproxrecip16x8;
    printf("16x8:\n");
    random_test_error();
    getapproxrecip = getapproxrecip8x8_32;
    printf("opt32 8x8:\n");
    random_test_error();
    getapproxrecip = getapproxrecip16x8_32;
    printf("opt32 16x8:\n");
    random_test_error();
}
