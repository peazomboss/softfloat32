#include "approxrecip.c"
#include <stdio.h>

__attribute__ ((noinline,sysv_abi)) uint64_t exactdiv64(uint64_t u, uint64_t v)
{
    asm("movq %rdi, %rdx\n");
    asm("xorq %rax, %rax\n");
    asm("divq %rsi\n");
}

uint64_t approxdiv64(uint64_t u, uint64_t v)
{
    uint64_t recip = getapproxrecip64_2(v);
    return (mulhu64(u, recip) << 1);
}

uint64_t recipdiv64(uint64_t u, uint64_t v)
{
    uint64_t r1, r0, q;
    q = mulhu64(u, getapproxrecip64_1(v)) << 1;
    r1 = u - mulhu64(q, v) - 1;
    r0 = -(q * v);
    while ((r1 != 0) || (r0 >= v)) {
        q++;
        if (r0 < v)
            r1--;
        r0 -= v;
    }
    return q;
}

int main()
{
    const uint64_t u = 0x8111222233334444;
    const uint64_t v = 0x8fffeeeeddddcccc;
    printf("recip  div64: %llx\n", recipdiv64(u, v));
    printf("exact  div64: %llx\n", exactdiv64(u, v));
    printf("approx div64: %llx\n", approxdiv64(u, v));
}
