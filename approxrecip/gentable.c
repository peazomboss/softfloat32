#include <stdio.h>
#include <stdint.h>

void gentable8x8()
{
    printf("TABLE 8x8:\n");
    for (int i=8; i<=15; i++) {
        uint32_t u = (i << 28) + 0x8000000;
        uint32_t x = (uint32_t)0x7fffffff / (u >> 24);
        uint16_t w = x;
        x >>= 16;
        if (w >= 0x8000)
            x++;
        printf("0x%x, ", x);
    }
    putchar(10);
}

void gentable16x8()
{
    printf("TABLE 16x8:\n");
    for (int i=0; i<=15; i++) {
        uint32_t u = 0x80000000 + (i << 27) + 0x4000000;
        uint32_t x = (uint32_t)0x7fffffff / (u >> 24);
        uint16_t w = x;
        x >>= 16;
        if (w >= 0x8000)
            x++;
        printf("0x%x, ", x);
        if ((i+1) % 8 == 0)
            putchar(10);
    }
    putchar(10);
}

int main()
{
    gentable8x8();
    gentable16x8();
}
