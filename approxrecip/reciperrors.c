#include <stdio.h>
#include "approxrecip.c"

uint32_t (*getapproxrecip)(uint32_t);

void bruteerrortest()
{
    int maxpos, maxneg, eq, neg1, neg2, neg3;
    maxpos = 0; maxneg = 0; eq = 0; neg1 = 0; neg2 = 0; neg3 = 0;
    for (uint32_t a = 0x80000000; (int32_t)a < 0; a++) {
        uint32_t recip1 = getapproxrecip(a);
        uint32_t recip2 = 0x7fffffffffffffff / a;
        int diff = recip1 - recip2;
        if (diff == 0)
            eq++;
        else if (diff == -1)
            neg1++;
        else if (diff == -2)
            neg2++;
        else if (diff == -3)
            neg3++;
        if (diff > maxpos)
            maxpos = diff;
        if (diff < maxneg)
            maxneg = diff;
    }
    printf("[eq] %d, [maxpos] %d, [maxneg] %d\n", eq, maxpos, maxneg);
    printf("[-1] %d, [-2] %d, [-3] %d\n", neg1, neg2, neg3);
}

void brutetest()
{
    getapproxrecip = getapproxrecip8x8;
    printf("8x8:\n");
    bruteerrortest();
    getapproxrecip = getapproxrecip16x8;
    printf("16x8:\n");
    bruteerrortest();
    getapproxrecip = getapproxrecip8x8_32;
    printf("opt32 8x8:\n");
    bruteerrortest();
    getapproxrecip = getapproxrecip16x8_32;
    printf("opt32 16x8:\n");
    bruteerrortest();
}

void findneg3()
{
    for (uint32_t a = 0x80000000; (int32_t)a < 0; a++) {
        uint32_t recip1 = getapproxrecip16x8(a);
        uint32_t recip2 = 0x7fffffffffffffff / a;
        int diff = recip1 - recip2;
        if (diff == -3)
            printf("%x\n", a);
    }
}

int main()
{
    brutetest();
}
