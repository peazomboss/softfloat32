#include <stdio.h>
#include "approxrecip.c"
#include "rng.h"

#ifndef __amd64__
#error "Require amd64(x86-64)"
#endif

uint64_t (*approxrecip64)(uint64_t);

__attribute__((noinline)) uint64_t exactrecip64(uint64_t n)
{
    asm("movq $0xffffffffffffffff, %rax\n");
    asm("movq $0x7fffffffffffffff, %rdx\n");
#ifdef _WIN64
    asm("divq %rcx\n");
#else
    asm("divq %rdi\n");
#endif
}

int64_t recip64diff(uint64_t n)
{
    uint64_t recip1 = exactrecip64(n);
    uint64_t recip2 = approxrecip64(n);
    return recip2 - recip1;
}

void recip64error()
{
    const int LOOP_MAX = 100000000;
    int maxpos, maxneg;
    maxpos = maxneg = 0;
    for (int i=0; i<LOOP_MAX; i++) {
        uint64_t n;
        n = rng_next() % 0x80000000 + 0x80000000;
        n = (n << 32) + rng_next();
        int64_t diff = recip64diff(n);
        if (diff > maxpos)
            maxpos = diff;
        if (diff < maxneg)
            maxneg = diff;
    }
    printf("[maxpos]: %d, [maxneg]: %d\n", maxpos, maxneg);
}

int main()
{
    rng_init();
    approxrecip64 = getapproxrecip64_1;
    recip64error();
    approxrecip64 = getapproxrecip64_2;
    recip64error();
}
