#include <stdint.h>
#include <time.h>

uint32_t rng_seed;

static inline void rng_init()
{
    rng_seed = time(NULL);
}

static inline uint32_t rng_next()
{
    rng_seed = rng_seed * 134775813 + 1;
    return rng_seed;
}
