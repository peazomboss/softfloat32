# 更新内容

## 迭代过程针对部分32位CPU的优化

这一部分主要是针对如下代码在部分32位CPU下的优化：

```cpp
uint32_t getapproxrecip8x8(uint32_t n)
{
    uint32_t x, t;
    x = initreciptable8x8[(n >> 28) & 7] << 24;
    t = ((uint64_t)x * n) >> 32;
    t = ~t;
    x = ((uint64_t)x * t) >> 31;
    t = ((uint64_t)x * n) >> 32;
    t = ~t;
    x = ((uint64_t)x * t) >> 31;
    t = ((uint64_t)x * n) >> 32;
    t = ~t;
    x = ((uint64_t)x * t) >> 31;
    return x;
}
```

仔细观察代码中的 `x = ((uint64_t)x * t) >> 31;` 这一部分，对于大部分32位CPU来说这句代码的执行需要较大的开销。对于乘法，有些32位CPU不一定支持32位相乘得到64位的操作，但是可以使用一定的算法计算高32位结果。除了乘法指令，右移31位一般需要的指令超过两条（除了Intel，刚好是两条），因为一个64位结果是保存在两个不同的寄存器的。而后面要讲的优化代码对于Intel的CPU来说可能效果不大。

考虑到牛顿迭代法的二次收敛性，其实一开始的两次迭代从4位到8位再到16位精度，实际上不需要右移31位来确保精度。所以完全可以在前两次迭代使用高32位（第63位到32位）的结果，然后最后一次结果才使用完整的第62到31位结果（即右移31位）。

改进后的代码如下（误差和原来一样）：

```cpp
uint32_t getapproxrecip8x8_32(uint32_t n)
{
    uint32_t x, t;
    x = initreciptable8x8[(n >> 28) & 7] << 24;
    t = ((uint64_t)x * n) >> 32;
    t = -t;
    x = ((uint64_t)x * t) >> 32;
    x = x << 1;
    t = ((uint64_t)x * n) >> 32;
    t = -t;
    x = ((uint64_t)x * t) >> 32;
    x = x << 1;
    t = ((uint64_t)x * n) >> 32;
    t = ~t;
    x = ((uint64_t)x * t) >> 31;
    return x;
}
```

这段代码对于编译器来说会生成更短的代码，可以提高执行速度。尽管按照之前的分析使用 `t = -t;` 会造成误差，但是这里用了高32位结果，忽略了1位，所以影响不大，而用 `t = ~t;` 甚至会过于偏小了，导致平均误差偏大。但是最后一次迭代必须确保结果不偏大，所以就需要使用 `t = ~t;` 了。

如果CPU不支持结果是64位的乘法，那么可以用如下代码（不过会导致最大误差达到-4）：

```cpp
uint32_t getapproxrecip8x8_32(uint32_t n)
{
    uint32_t x, t;
    x = initreciptable8x8[(n >> 28) & 7] << 24;
    t = -mulhu(x, n);
    x = mulhu(x, t) << 1;
    t = -mulhu(x, n);
    x = mulhu(x, t) << 1;
    t = ~mulhu(x, n);
    x = mulhu(x, t) << 1;
    return x;
}
```

这段用到的 `mulhu` 函数的作用是计算两个32位无符号整数的乘积，返回乘积的高32位结果。具体代码的实现，可以在 Hacker's Delight 第 8.2 节找到。

上述代码都经过穷举测试，应该是效果比较好的了。

## 计算64位定点小数的近似倒数

要计算Q1.63的近似倒数，只需在Q1.31近似倒数的基础上再进行一次或两次迭代就行了，一种可以用于64位CPU和大多数32位CPU的方法如下：

```cpp
uint64_t getapproxrecip64_1(uint64_t n)
{
    uint64_t x, t;
    x = (uint64_t)getapproxrecip16x8(n >> 32) << 32;
    t = ~mulhu64(x, n);
    x = mulhu64(x, t) << 1;
    return x;
}

uint64_t getapproxrecip64_2(uint64_t n)
{
    uint64_t x, t;
    x = (uint64_t)getapproxrecip8x8(n >> 32) << 32;
    t = ~mulhu64(x, n);
    x = mulhu64(x, t) << 1;
    t = ~mulhu64(x, n);
    x = mulhu64(x, t) << 1;
    return x;
}
```

注意一次迭代为了精度，使用的是 `getapproxrecip16x8` 来获取32位近似倒数，而两次迭代则无所谓了。

其中的 `mulhu64` 的实现如下：

```cpp
uint64_t mulhu64(uint64_t u, uint64_t v)
{
    uint64_t w0,w1,w2,u0,u1,v0,v1,t;
    u0 = u & 0xffffffff;
    u1 = u >> 32;
    v0 = v & 0xffffffff;
    v1 = v >> 32;
    w0 = u0 * v0;
    t = u1 * v0 + (w0 >> 32);
    w1 = t & 0xffffffff;
    w2 = t >> 32;
    w1 = u0 * v1 + w1;
    return u1 * v1 + w2 + (w1 >> 32);
}
```

这样就能得到精度尚可的Q64小数结果。

为了分析误差，需要计算精确的倒数，这对于x86-64架构来说丝毫没有难度，然而对于大多数CPU来说确存在问题。为了简化操作，就只选择使用x86-64来进行分析了，否则不光实现复杂，耗时也会很长。

以下代码可以获得精确的Q1.63小数的Q64倒数：

```cpp
__attribute__((noinline)) uint64_t exactrecip64(uint64_t n)
{
    asm("movq $0xffffffffffffffff, %rax\n");
    asm("movq $0x7fffffffffffffff, %rdx\n");
#ifdef _WIN64
    asm("divq %rcx\n");
#else
    asm("divq %rdi\n");
#endif
}
```

之后用随机数进行误差分析，可以得到大致的误差范围。

一次迭代的倒数误差在 $[-12,0]$ 之间，两次迭代则缩小到了 $[-3,0]$ 之间。

## 计算64位定点小数除法

在得到倒数以后，显然可以去计算定点小数的除法了，为了和精确结果比较，所以需要计算精确的结果，和之前一样，用x86-64汇编来实现：

```cpp
__attribute__ ((noinline,sysv_abi))
uint64_t exactdiv64(uint64_t u, uint64_t v)
{
    asm("movq %rdi, %rdx\n");
    asm("xorq %rax, %rax\n");
    asm("divq %rsi\n");
}
```

为了不针对不同平台写两份汇编，就用了 `sysv_abi` 这个调用约定，这样前两个参数就是 `rdi` 和 `rsi` 了。

为了节约篇幅，下面就直接贴出计算除法以及修正商的代码：

```cpp
uint64_t recipdiv64(uint64_t u, uint64_t v)
{
    uint64_t r1, r0, q;
    q = mulhu64(u, getapproxrecip64_1(v)) << 1;
    r1 = u - mulhu64(q, v) - 1;
    r0 = -(q * v);
    while ((r1 != 0) || (r0 >= v)) {
        q++;
        if (r0 < v)
            r1--;
        r0 -= v;
    }
    return q;
}
```

由于没有128位结果的乘法以及128位的加减法，所以需要拆开来处理，这个实际上不难实现。上述代码中变量 `r1` 在32位CPU实现可以用 `uint32_t` 类型，其它就没什么了。

有了64位定点小数的实现，实际上实现一个 `double` 类型浮点数的模拟也就不是什么难事了。
