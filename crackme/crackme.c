#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "softfloat32.h"
#include "rng.h"
#include "approxrecip.h"

static const int STR_ERR[] = {
    0x78911a, 0xc73284, 0xc73284, 0xc1f48e, 0xc73284, 0x4ce218,
    0x37ea40, 0xc3b3e0, 0xbcb698, 0xb07b5a, 0xa97e12, 0xc8f1d6,
    0xb07b5a, 0x37ea40, 0xcab128, 0xc73284, 0xd36dc2, 0x37ea40,
    0xa97e12, 0xb3f9fe, 0xa97e12, 0xb778a2, 0xc0353c, 0x5060bc,
};

static int count_leading_zero(uint32_t x)
{
    if (x == 0)
        return 32;
    int n = 1;
    if ((x >> 16) == 0) { n += 16; x <<= 16; }
    if ((x >> 24) == 0) { n += 8; x <<= 8; }
    if ((x >> 28) == 0) { n += 4; x <<= 4; }
    if ((x >> 30) == 0) { n += 2; x <<= 2; }
    n -= (x >> 31);
    return n;
}

static uint32_t udiv(uint32_t a, uint32_t b)
{
    if (a < b)
        return 0;
    int lz = count_leading_zero(b);
    uint32_t recip_b = getapproxrecip16x8_32(b << lz);
    uint32_t quo = ((uint64_t)a * recip_b) >> (63 - lz);
    uint32_t rem = a - quo * b;
    while (rem >= b) {
        quo++;
        rem -= b;
    }
    return quo;
}

static void show_error()
{
    for (int i = 0; i < 24; i++) {
        int c = STR_ERR[i];
        c = udiv(c, 114514);
        putchar(c);
    }
    putchar('\n');
}

static const int STR_OK[] = {
    0x3f1f3e7d, 0x3f172e5d, 0x3eb162c6, 0x3e810204, 0x3f73e7d0, 0x3f5fbf7f,
    0x3f6bd7af, 0x3e810204, 0x3f43870e, 0x3f65cb97, 0x3f4b972e, 0x3e810204,
    0x3f65cb97, 0x3f53a74f, 0x3f4f9f3e, 0x3f51a347, 0x3f69d3a7, 0x3eb972e6,
};

static void show_ok()
{
    for (int i = 0; i < 18; i++) {
        sfloat32 res = sf32_mul(STR_OK[i], sf32_from_i32(127));
        int c = sf32_to_i32(res);
        putchar(c);
    }
    putchar('\n');
}

static const int STR_IN_UID[] = {
    0x3ea0697a, 0x3ef1b77c, 0x3ef61c91, 0x3f008ca3, 0x3efee6bb, 0x3e0ca29c,
    0x3f008ca3, 0x3ee6bac8, 0x3edbbe14, 0x3e7ee6bb, 0x3e0ca29c,
};

static void show_input_uid() {
    for (int i = 0; i < 11; i++) {
        sfloat32 res = sf32_div(STR_IN_UID[i], sf32_from_float(1.0 / 233));
        int c = sf32_to_i32(res);
        putchar(c);
    }
}

static const int STR_IN_FLAG[] = {
    0x35b7bf3e, 0x50f1cf84, 0x526a9220, 0x561878a6, 0x555c1758, 0x178c29c0,
    0x4b0ec514, 0x4f790ce8, 0x4760de8e, 0x4bcb2662, 0x2aae0bac, 0x178c29c0,
};

static void show_input_flag()
{
    for (int i = 0; i < 12; i++) {
        int c = STR_IN_FLAG[i];
        c = udiv(c, 12345678);
        putchar(c);
    }
}

static const int HEX_DIG[] = {
    0x39a50, 0x3ad87, 0x3c0be, 0x3d3f5, 0x3e72c, 0x3fa63,
    0x40d9a, 0x420d1, 0x43408, 0x4473f, 0x747d7, 0x75b0e,
    0x76e45, 0x7817c, 0x794b3, 0x7a7ea,
};

static inline char decode_hex(int i)
{
    int dig = HEX_DIG[i & 0xf];
    return udiv(dig, 0x1337);
}

static const int STR_PAUSE[] = {
    0x3e5c8dc9, 0x3e3f03f0, 0x3e666666, 0x3e627627, 0x3e46e46e,
};

void sys_pause()
{
    char pause[6] = { 0 };
    for (int i = 0; i < 5; i++) {
        sfloat32 res = sf32_mul(STR_PAUSE[i], sf32_from_i32(520));
        int c = sf32_to_i32(res);
        pause[i] = c;
    }
    system(pause);
}

int check(int uid, const char* flag)
{
    sfloat32 norm_uid = sf32_div(sf32_from_i32(uid), sf32_from_i32(3000000));
    norm_uid = sf32_sub(norm_uid, sf32_from_float(0.5));
    if (sf32_le(norm_uid, 0))
        norm_uid++;
    else
        norm_uid--;
    // printf("%x\n", norm_uid);
    sfloat32 mul_1ov52 = sf32_mul(sf32_from_i32(uid), sf32_from_float(1.0 / 52));
    // printf("%.7f\n", sf32_to_float(mul_1ov52));
    int i_part = sf32_to_i32(mul_1ov52);
    if (sf32_gt(mul_1ov52, sf32_from_i32(28846)))
        i_part >>= 1;
    i_part += 1024;
    rng_seed = norm_uid;
    for (int i = 1; i < i_part; i++)
        rng_next();
    uint32_t num1 = rng_next();
    uint32_t num2 = num1 + norm_uid + rng_next();
    // printf("num1: %x, num2: %x\n", num1, num2);
    num2 = (num2 << 16) | (num2 >> 16);
    for (int i = 0; i < 8; i++) {
        int c = flag[i];
        char dig = decode_hex(num1);
        num1 >>= 4;
        if (c != dig)
            return 0;
    }
    for (int i = 8; i < 16; i++) {
        int c = flag[i];
        char dig = decode_hex(num2);
        num2 >>= 4;
        if (c != dig)
            return 0;
    }
    return 1;
}

int main()
{
    int uid;
    char flag[128];
    show_input_uid();
    if (scanf("%d", &uid) == 0)
        goto l_bad;
    if (uid <= 0 || uid >= 3000000)
        goto l_bad;
    show_input_flag();
    scanf("%s", flag);
    if (strlen(flag) != 16)
        goto l_bad;
    if (!check(uid, flag))
        goto l_bad;
l_good:
    show_ok();
    goto l_end;
l_bad:
    show_error();
l_end:
    sys_pause();
}

