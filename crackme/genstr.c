#include <stdio.h>

const char* STR_ERR = "Error, please try again.";
const char* STR_OK  = "OK, you are right.";
const char* STR_IN_UID = "Input uid: ";
const char* STR_IN_FLAG = "Input flag: ";
const char* HEX_DIG = "0123456789abcdef";
const char* STR_PAUSE = "pause";

void encrypt_float(const char* str, float div)
{
    const char* p = str;
    int count = 0;
    while (*p) {
        count++;
        union {
            float f;
            int i;
        } num;
        num.f = (float)*p / div;
        printf("0x%x, ", num.i);
        if (count % 6 == 0)
            putchar('\n');
        p++;
    }
    putchar('\n');
}

void encrypt_int(const char* str, int magic)
{
    const char* p = str;
    int count = 0;
    while (*p) {
        count++;
        int num = (int)*p * magic;
        printf("0x%x, ", num);
        if (count % 6 == 0)
            putchar('\n');
        p++;
    }
    putchar('\n');
}

int main()
{
    encrypt_int(STR_ERR, 114514);
    encrypt_float(STR_OK, 127);
    encrypt_float(STR_IN_UID, 233);
    encrypt_int(STR_IN_FLAG, 12345678);
    encrypt_int(HEX_DIG, 0x1337);
    encrypt_float(STR_PAUSE, 520);
}
