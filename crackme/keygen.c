#include <stdio.h>
#include <string.h>
#include "../approxrecip/rng.h"

static const char HEX_DIG[] = "0123456789abcdef";

int main()
{
    int uid;
    printf("uid: ");
    scanf("%d", &uid);
    union
    {
        float f;
        uint32_t i;
    } norm_uid;
    norm_uid.f = (float)uid / 3000000;
    norm_uid.f -= 0.5;
    if (norm_uid.f <= 0)
        norm_uid.i++;
    else
        norm_uid.i--;
    printf("%x\n", norm_uid.i);
    float mul_1ov52 = (float)uid * (1.0 / 52);
    printf("%.7f\n", mul_1ov52);
    int i_part = mul_1ov52;
    if (mul_1ov52 > 28846)
        i_part >>= 1;
    i_part += 1024;
    rng_seed = norm_uid.i;
    for (int i = 1; i < i_part; i++)
        rng_next();
    uint32_t num1 = rng_next();
    uint32_t num2 = num1 + norm_uid.i + rng_next();
    num2 = (num2 << 16) | (num2 >> 16);
        printf("num1: %x, num2: %x\n", num1, num2);
    printf("flag: ");
    for (int i = 0; i < 8; i++) {
        putchar(HEX_DIG[num1 & 0xf]);
        num1 >>= 4;
    }
    for (int i = 0; i < 8; i++) {
        putchar(HEX_DIG[num2 & 0xf]);
        num2 >>= 4;
    }
    putchar('\n');
}