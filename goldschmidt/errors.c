#include <stdio.h>
#include "goldschmidt.h"

uint32_t (*getapproxrecip)(uint32_t);

void bruteerrortest()
{
    int maxpos, maxneg, eq, neg1, neg2, neg3;
    maxpos = 0; maxneg = 0; eq = 0; neg1 = 0; neg2 = 0; neg3 = 0;
    for (uint32_t a = 0x80000000; (int32_t)a < 0; a++) {
        uint32_t recip1 = getapproxrecip(a);
        uint32_t recip2 = 0x3fffffffffffffff / a;
        int diff = recip1 - recip2;
        if (diff == 0)
            eq++;
        else if (diff == -1)
            neg1++;
        else if (diff == -2)
            neg2++;
        else if (diff == -3)
            neg3++;
        if (diff > maxpos)
            maxpos = diff;
        if (diff < maxneg)
            maxneg = diff;
    }
    printf("[eq] %d, [maxpos] %d, [maxneg] %d\n", eq, maxpos, maxneg);
    printf("[-1] %d, [-2] %d, [-3] %d\n", neg1, neg2, neg3);
}

static uint32_t _recip_gs_table_64(uint32_t n)
{
    return recip_gs_table_64(n);
}

static uint32_t _recip_gs_table_128(uint32_t n)
{
    return recip_gs_table_128(n);
}

void brutetest()
{
    getapproxrecip = _recip_gs_table_64;
    printf("table 64:\n");
    bruteerrortest();
    getapproxrecip = _recip_gs_table_128;
    printf("table 128:\n");
    bruteerrortest();
}

int main()
{
    brutetest();
}
