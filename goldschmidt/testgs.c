#include <stdint.h>
#include <stdio.h>
#include <math.h>
#include "goldschmidt.h"

double q1_31_to_float(uint32_t q)
{
    return (double)q / (double)0x80000000;
}

uint32_t float_to_q1_31(double f)
{
    return (uint32_t)round((double)0x80000000 * f);
}

void test_div()
{
    show_goldschmidt_debug_info = 1;
    const uint32_t x = 0x28888888;
    const uint32_t d = 0x82222222;
    uint32_t q = fixed_div_gs(x, d);
    const double f_x = q1_31_to_float(x);
    const double f_d = q1_31_to_float(d);
    const double f_q = q1_31_to_float(q);
    printf("%.15f / %.15f = %.15f (0x%x) vs %.15f (0x%x)\n",
        f_x,
        f_d,
        f_q,
        q,
        f_x / f_d,
        float_to_q1_31(f_x / f_d)
    );
}

static uint32_t real_recip(uint32_t n)
{
    return float_to_q1_31((double)0x80000000 / n);
}

void test_recip(uint32_t v)
{
    const uint32_t r = recip_gs(v);
    printf("0x%x, %.15f\n", r, q1_31_to_float(r));
}

void test_recip_64(uint32_t v)
{
    const uint32_t r = recip_gs_table_64(v);
    printf("0x%x, %.15f\n", r, q1_31_to_float(r));
}

void test_recip_128(uint32_t v)
{
    const uint32_t r = recip_gs_table_128(v);
    printf("0x%x, %.15f\n", r, q1_31_to_float(r));
}

void test_all_recips()
{
    const uint32_t v = 0x82222222;
    test_recip(v);
    test_recip_64(v);
    test_recip_128(v);
    printf("0x%x\n", real_recip(v));
}

int main()
{
    show_goldschmidt_debug_info = 1;
    test_div();
    test_all_recips();
}
