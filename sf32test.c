/*  测试代码，仅用于展示目的
    Mingw编译命令：
        $ gcc sf32test.c -o sf32test -O3
        $ gcc sf32test.c -o sf32test_trunc -O3 -DROUND_TRUNC
    Linux下命令为：
        $ gcc sf32test.c -o sf32test -O3
        $ gcc sf32test.c -o sf32test_trunc -O3 -DROUND_TRUNC -lm
*/
#include <stdio.h> // printf
#include <fenv.h>  // fesetround
// #define ROUND_TRUNC
#ifdef MAKEFILE
#include "softfloat32.h"
#else
#include "softfloat32.c"
#endif

#define INF (1.f / 0.f)
#define NAN (0.f / 0.f)

static char *if_true_false(bool b)
{
    return b ? "true" : "false";
}

static void test_cmp(float f1, float f2)
{
    sfloat32 sf1 = sf32_from_float(f1);
    sfloat32 sf2 = sf32_from_float(f2);
    printf("%f == %f [%s] \n", f1, f2, if_true_false(sf32_eq(sf1, sf2)));
    printf("%f <  %f [%s] \n", f1, f2, if_true_false(sf32_lt(sf1, sf2)));
    printf("%f <= %f [%s] \n", f1, f2, if_true_false(sf32_le(sf1, sf2)));
    printf("%f != %f [%s] \n", f1, f2, if_true_false(sf32_ne(sf1, sf2)));
    printf("%f >  %f [%s] \n", f1, f2, if_true_false(sf32_gt(sf1, sf2)));
    printf("%f >= %f [%s] \n", f1, f2, if_true_false(sf32_ge(sf1, sf2)));
    printf("\n");
}

static void test_cmps()
{
    test_cmp(0, 1);
    test_cmp(1, INF);
    test_cmp(1, -INF);
    test_cmp(INF, INF);
    test_cmp(INF, -INF);
    test_cmp(INF, NAN);
    test_cmp(NAN, NAN);
    test_cmp(0, NAN);
    test_cmp(+0, -0);
    printf("\n");
}

static void test_add(float f1, float f2)
{
    float f = f1 + f2;
    sfloat32 sf1 = sf32_from_float(f1);
    sfloat32 sf2 = sf32_from_float(f2);
    sfloat32 sf = sf32_add(sf1, sf2);
    printf("%.10e + %.10e = [hard]%.10e, [soft]%.10e\n", f1, f2, f, sf32_to_float(sf));
}

static void test_adds()
{
    test_add(1.1, 2.2);
    test_add(3e38, 2e38);
    test_add(1.1, 0);
    test_add(2e20, 2);
    test_add(2, INF);
    test_add(INF, 100);
    test_add(NAN, NAN);
    test_add(NAN, INF);
    test_add(INF, NAN);
    test_add(NAN, 2);
    test_add(100, NAN);
    printf("\n");
}

static void test_sub(float f1, float f2)
{
    float f = f1 - f2;
    sfloat32 sf1 = sf32_from_float(f1);
    sfloat32 sf2 = sf32_from_float(f2);
    sfloat32 sf = sf32_sub(sf1, sf2);
    printf("%.10e - %.10e = [hard]%.10e, [soft]%.10e\n", f1, f2, f, sf32_to_float(sf));
}

static void test_subs()
{
    test_sub(1.1, 2.2);
    test_sub(1.1, 0);
    test_sub(2e20, 2);
    test_sub(2, INF);
    test_sub(INF, 100);
    test_sub(NAN, NAN);
    test_sub(NAN, INF);
    test_sub(INF, NAN);
    test_sub(NAN, 2);
    test_sub(100, NAN);
    printf("\n");
}

static void test_mul(float f1, float f2)
{
    float f = f1 * f2;
    sfloat32 sf1 = sf32_from_float(f1);
    sfloat32 sf2 = sf32_from_float(f2);
    sfloat32 sf = sf32_mul(sf1, sf2);
    printf("%.10e * %.10e = [hard]%.10e, [soft]%.10e\n", f1, f2, f, sf32_to_float(sf));
}

static void test_muls()
{
    test_mul(1.1, 2.2);
    test_mul(1.1, 0);
    test_mul(2e20, 2);
    test_mul(2, INF);
    test_mul(INF, 100);
    test_mul(NAN, NAN);
    test_mul(NAN, INF);
    test_mul(INF, NAN);
    test_mul(NAN, 2);
    test_mul(100, NAN);
    test_mul(0, INF);
    test_mul(INF, 0);
    test_mul(2e20, 2e20);
    printf("\n");
}

static void test_div(float f1, float f2)
{
    float f = f1 / f2;
    sfloat32 sf1 = sf32_from_float(f1);
    sfloat32 sf2 = sf32_from_float(f2);
    sfloat32 sf = sf32_div(sf1, sf2);
    printf("%.10e / %.10e = [hard]%.10e, [soft]%.10e\n", f1, f2, f, sf32_to_float(sf));
}

static void test_divs()
{
    test_div(1.1, 2.2);
    test_div(1.1, 0);
    test_div(2e20, 2);
    test_div(2, INF);
    test_div(INF, 100);
    test_div(NAN, NAN);
    test_div(NAN, INF);
    test_div(INF, NAN);
    test_div(NAN, 2);
    test_div(100, NAN);
    test_div(0, INF);
    test_div(INF, 0);
    printf("\n");
}

static void test_conv_sf_to_i(float f)
{
    sfloat32 sf = sf32_from_float(f);
    int i = sf32_to_i32(sf);
    printf("int of %f = %d\n", f, i);
}

static void test_conv_i_to_sf(int i)
{
    sfloat32 sf = sf32_from_i32(i);
    float f = sf32_to_float(sf);
    printf("float from %d = %f\n", i, f);
}

static void test_convs()
{
    test_conv_sf_to_i(-1);
    test_conv_sf_to_i(0);
    test_conv_sf_to_i(0.5);
    test_conv_sf_to_i(2.5);
    test_conv_sf_to_i(8388607);
    test_conv_sf_to_i(8388608);
    test_conv_sf_to_i(8388609);
    test_conv_sf_to_i(16777217); // == 16777216
    test_conv_sf_to_i(1e20); // 溢出
    test_conv_sf_to_i(-1e20);
    printf("\n");
    test_conv_i_to_sf(-1);
    test_conv_i_to_sf(0);
    test_conv_i_to_sf(2);
    test_conv_i_to_sf(0x7fffff);
    test_conv_i_to_sf(0x800000);
    test_conv_i_to_sf(0x800001);
    test_conv_i_to_sf(0x8fffff);
    test_conv_i_to_sf(0x1000000);
    test_conv_i_to_sf(0x1000001); // == 16777216
    printf("\n");
}

int main()
{
#ifdef ROUND_TRUNC
    fesetround(FE_TOWARDZERO);
#endif
    test_cmps();
    test_adds();
    test_subs();
    test_muls();
    test_divs();
    test_convs();
}
