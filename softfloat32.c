#include "softfloat32.h"

#define SF32_INF 0x7f800000 // Inf，指数为0xff，有效位为0
#define SF32_NAN 0xffc00000 // NaN，指数为0xff，有效位不为0
#define SF32_MAX 0x7f7fffff // 单精度浮点表示的最大值

#define SF32_EXP(sf) (((sf) >> 23) & 0xff) // 取8位指数
#define SF32_SIG(sf) ((sf) & 0x7fffff) // 取23位有效位
#define SF32_SIGN(sf) ((sf) >> 31) // 取符号位

/* 构造一个32位浮点数 */
static inline sfloat32 sf32_pack(bool sign, uint16_t exp, uint32_t sig)
{
    return ((uint32_t)sign << 31) | ((uint32_t)exp << 23) | (sig & 0x7fffff);
}

inline sfloat32 sf32_from_float(float f)
{
    union
    {
        float f;
        uint32_t u;
    } usf;
    usf.f = f;
    return usf.u;
}

inline float sf32_to_float(sfloat32 sf)
{
    union
    {
        float f;
        uint32_t u;
    } usf;
    usf.u = sf;
    return usf.f;
}

inline bool sf32_isnan(sfloat32 sf)
{
    return (sf & 0x7fffffff) > SF32_INF;
}

inline bool sf32_isinf(sfloat32 sf)
{
    return (sf & 0x7fffffff) == SF32_INF;
}

inline bool sf32_sign(sfloat32 sf)
{
    return SF32_SIGN(sf);
}

static bool sf32_real_lt(sfloat32 sf1, sfloat32 sf2)
{
    bool sign1 = SF32_SIGN(sf1);
    bool sign2 = SF32_SIGN(sf2);
    if (sign1 != sign2) // 符号不同，也可以用异或判断
        // 若左边为负数且两数不为0，则说明左边小于右边
        return sign1 && ((sf1 | sf2) << 1);
    // 若符号相同，则左边小于右边的条件是两数不相等
    // 同时，若两数为正数，绝对值小的小，否则绝对值大的小
    return (sf1 != sf2) && (sign1 ^ (sf1 < sf2));
}

static bool sf32_real_le(sfloat32 sf1, sfloat32 sf2)
{
    bool sign1 = SF32_SIGN(sf1);
    bool sign2 = SF32_SIGN(sf2);
    if (sign1 != sign2)
        // 与之前不同，两数可以为0
        return sign1 || (((sf1 | sf2) << 1) == 0);
    // 与之前不同，相等或小于
    return (sf1 == sf2) || (sign1 ^ (sf1 < sf2));
}

bool sf32_eq(sfloat32 sf1, sfloat32 sf2)
{
    if (sf32_isnan(sf1) || sf32_isnan(sf2))
        return false;
    return
        (sf1 == sf2) || // 内容完全一样
        (((sf1 | sf2) << 1) == 0); // 对于0则忽略符号位
}

bool sf32_ne(sfloat32 sf1, sfloat32 sf2)
{
    return !sf32_eq(sf1, sf2);
}

bool sf32_lt(sfloat32 sf1, sfloat32 sf2)
{
    if (sf32_isnan(sf1) || sf32_isnan(sf2))
        return false;
    return sf32_real_lt(sf1, sf2);
}

bool sf32_le(sfloat32 sf1, sfloat32 sf2)
{
    if (sf32_isnan(sf1) || sf32_isnan(sf2))
        return false;
    return sf32_real_le(sf1, sf2);
}

bool sf32_gt(sfloat32 sf1, sfloat32 sf2)
{
    if (sf32_isnan(sf1) || sf32_isnan(sf2))
        return false;
    return !sf32_real_le(sf1, sf2);
}

bool sf32_ge(sfloat32 sf1, sfloat32 sf2)
{
    if (sf32_isnan(sf1) || sf32_isnan(sf2))
        return false;
    return !sf32_real_lt(sf1, sf2);
}

// 对于32位数，右移保留粘贴位
static uint32_t shift_right_sticky32(uint32_t sig, uint16_t n)
{
    uint32_t res = 0;
    if (n < 31) {
        res = sig >> n;
        if (sig << (32 - n))
            res = res | 1;
    }
    else {
        if (sig)
            res = 1;
    }
    return res;
}

// 对于64位数，右移保留粘贴位
static uint64_t shift_right_sticky64(uint64_t sig, uint16_t n)
{
    uint64_t res = 0;
    if (n < 63) {
        res = sig >> n;
        if (sig << (64 - n))
            res = res | 1;
    }
    else {
        if (sig)
            res = 1;
    }
    return res;
}

// 对于32位数，进行向偶数舍入操作，低3位分别为GRS位
static uint32_t round_even_grs_32(uint32_t sig)
{
    uint8_t grs = sig & 7;
    sig = sig >> 3;
    if ((grs > 4) || ((grs == 4) && (sig & 1)))
        sig++;
    return sig;
}

// 判断NaN或Inf
static inline sfloat32 sf32_nan_inf(bool sign, uint32_t sig)
{
    if (sig)
        return SF32_NAN;
    return (SF32_INF | ((uint32_t)sign << 31));
}

// 无符号长除法，返回商和余数
// 输入参数需要确保结果不超过32位宽度，确保余数指针不为NULL
static inline uint32_t ulong_div_mod(uint64_t n, uint32_t base, uint32_t *r)
{
    uint32_t t, x, y; // x高32位，y低32位
    x = n >> 32;
    y = n;
    for (int i = 1; i <= 32; i++) {
        t = x >> 31;
        x = (x << 1) | (y >> 31);
        y = y << 1;
        if ((x | t) >= base) {
            x -= base;
            y++;
        }
    }
    *r = x;
    return y;
}

// 无符号长除法，但是忽略余数
static inline uint32_t ulong_div(uint64_t n, uint32_t base)
{
    uint32_t t, x, y; // x高32位，y低32位
    x = n >> 32;
    y = n;
    for (int i = 1; i <= 32; i++) {
        t = x >> 31;
        x = (x << 1) | (y >> 31);
        y = y << 1;
        if ((x | t) >= base) {
            x -= base;
            y++;
        }
    }
    return y;
}

static const uint8_t initreciptable8x8[8] = {
    0xf1, 0xd8, 0xc3, 0xb2, 0xa4, 0x98, 0x8d, 0x84
};

static inline uint32_t approx_recip(uint32_t n)
{
    uint32_t x, t;
    x = initreciptable8x8[(n >> 28) & 7] << 24;
    t = ((uint64_t)x * n) >> 32;
    t = ~t;
    x = ((uint64_t)x * t) >> 31;
    t = ((uint64_t)x * n) >> 32;
    t = ~t;
    x = ((uint64_t)x * t) >> 31;
    t = ((uint64_t)x * n) >> 32;
    t = ~t;
    x = ((uint64_t)x * t) >> 31;
    return x;
}

static int count_leading_zero(uint32_t x)
{
    if (x == 0)
        return 32;
    int n = 1;
    if ((x >> 16) == 0) { n += 16; x <<= 16; }
    if ((x >> 24) == 0) { n += 8; x <<= 8; }
    if ((x >> 28) == 0) { n += 4; x <<= 4; }
    if ((x >> 30) == 0) { n += 2; x <<= 2; }
    n -= (x >> 31);
    return n;
}

/* 加法，两数绝对值相加，符号取决于第一个参数 */
static sfloat32 real_sf32_add(sfloat32 sf1, sfloat32 sf2)
{
    int exp1 = SF32_EXP(sf1);
    int exp2 = SF32_EXP(sf2);
    uint32_t sig1 = SF32_SIG(sf1);
    uint32_t sig2 = SF32_SIG(sf2);
    bool sign = SF32_SIGN(sf1);
    int expdiff = exp1 - exp2;
    if (expdiff == 0) { // 两数指数相同
        if (exp1 == 0) // 忽略非规格化数
            return ((uint32_t)sign << 31);
        if (exp1 == 0xff) // 判断无穷或NaN
            return sf32_nan_inf(sign, sig1 | sig2);
    }
    if (exp1 == 0xff)
        return sf32_nan_inf(sign, sig1);
    if (exp2 == 0xff)
        return sf32_nan_inf(sign, sig2);
    if (exp1 == 0)
        return (sf2 & 0x7fffffff) | ((uint32_t)sign << 31);
    if (exp2 == 0)
        return (sf1 & 0x7fffffff) | ((uint32_t)sign << 31);
    sig1 |= 0x800000;
    sig2 |= 0x800000;
#ifndef ROUND_TRUNC
    sig1 = sig1 << 2;
    sig2 = sig2 << 2;
#endif
    if (expdiff < 0) {
        expdiff = -expdiff;
#ifdef ROUND_TRUNC
        if (expdiff > 24)
            sig1 = 0;
        else
            sig1 = sig1 >> expdiff;
#else
        sig1 = shift_right_sticky32(sig1, expdiff);
#endif
        exp1 = exp2;
    }
    else if (expdiff > 0) {
#ifdef ROUND_TRUNC
        if (expdiff > 24)
            sig2 = 0;
        else
            sig2 = sig2 >> expdiff;
#else
        sig2 = shift_right_sticky32(sig2, expdiff);
#endif
    }
    uint32_t sig = sig1 + sig2;
#ifdef ROUND_TRUNC
    if (sig > 0xffffff) {
        sig = sig >> 1;
        exp1++;
    }
#else
    if (sig < 0x4000000)
        sig = sig << 1;
    else
        exp1++;
    sig = round_even_grs_32(sig);
    if (sig > 0xffffff)
        exp1++;
#endif
    if (exp1 > 254)
#ifdef ROUND_TRUNC
        return (SF32_MAX | ((uint32_t)sign << 31));
#else
        return (SF32_INF | ((uint32_t)sign << 31));
#endif
    return sf32_pack(sign, exp1, sig);
}

/* 减法，两数绝对值相减，符号为绝对值大的那个 */
static sfloat32 real_sf32_sub(sfloat32 sf1, sfloat32 sf2)
{
    int exp1 = SF32_EXP(sf1);
    int exp2 = SF32_EXP(sf2);
    uint32_t sig1 = SF32_SIG(sf1);
    uint32_t sig2 = SF32_SIG(sf2);
    uint32_t sig;
    bool sign = SF32_SIGN(sf1);
    int expdiff = exp1 - exp2;
    if (expdiff == 0) {
        if (exp1 == 0xff)
            return SF32_NAN;
        if (sig1 == sig2)
            return 0;
        if (sig1 > sig2) {
            sig = sig1 - sig2;
        }
        else {
            sig = sig2 - sig1;
            sign = !sign;
        }
        sig = sig << 3;
    }
    else if (expdiff < 0) {
        sign = !sign;
        if (exp2 == 0xff)
            return sf32_nan_inf(sign, sig2);
        if (exp1 == 0)
            return (sf2 & 0x7fffffff) | ((uint32_t)sign << 31);
        exp1 = exp2;
        expdiff = -expdiff;
        sig1 = (sig1 | 0x800000) << 3;
        sig1 = shift_right_sticky32(sig1, expdiff);
        sig = ((sig2 | 0x800000) << 3) - sig1;
    }
    else {
        if (exp1 == 0xff)
            return sf32_nan_inf(sign, sig1);
        if (exp2 == 0)
            return (sf1 & 0x7fffffff) | ((uint32_t)sign << 31);
        sig2 = (sig2 | 0x800000) << 3;
        sig2 = shift_right_sticky32(sig2, expdiff);
        sig = ((sig1 | 0x800000) << 3) - sig2;
    }
#if 1 // 使用前导0的方法
    int shift = count_leading_zero(sig) - 5;
    exp1 -= shift;
    sig <<= shift;
#else // 不使用前导0
    while (sig < 0x4000000) {
        sig = sig << 1;
        exp1--;
    }
#endif
#ifdef ROUND_TRUNC
    sig = sig >> 3;
#else
    sig = round_even_grs_32(sig);
    if (sig > 0xffffff)
        exp1++;
#endif
    if (exp1 < 1)
        return ((uint32_t)sign << 31);
    return sf32_pack(sign, exp1, sig);
}

sfloat32 sf32_add(sfloat32 sf1, sfloat32 sf2)
{
    if (SF32_SIGN(sf1) == SF32_SIGN(sf2))
        return real_sf32_add(sf1, sf2);
    return real_sf32_sub(sf1, sf2);
}

sfloat32 sf32_sub(sfloat32 sf1, sfloat32 sf2)
{
    if (SF32_SIGN(sf1) == SF32_SIGN(sf2))
        return real_sf32_sub(sf1, sf2);
    return real_sf32_add(sf1, sf2);
}

sfloat32 sf32_mul(sfloat32 sf1, sfloat32 sf2)
{
    int exp1 = SF32_EXP(sf1);
    int exp2 = SF32_EXP(sf2);
    uint32_t sig1 = SF32_SIG(sf1);
    uint32_t sig2 = SF32_SIG(sf2);
    bool sign = SF32_SIGN(sf1) ^ SF32_SIGN(sf2);
    if (exp1 == 0xff) {
        if (sig1 || ((exp2 == 0xff) && sig2) || (exp2 == 0))
            return SF32_NAN;
        return (SF32_INF | ((uint32_t)sign << 31));
    }
    if (exp2 == 0xff) {
        if (sig2 || (exp1 == 0))
            return SF32_NAN;
        return (SF32_INF | ((uint32_t)sign << 31));
    }
    if ((exp1 == 0) || (exp2 == 0))
        return ((uint32_t)sign << 31);
    uint32_t sig;
    int exp = exp1 + exp2 - 127;
    sig1 |= 0x800000;
    sig2 |= 0x800000;
#ifdef ROUND_TRUNC
    sig = ((uint64_t)sig1 * sig2) >> 23;
    if (sig > 0xffffff) {
        sig = sig >> 1;
        exp++;
    }
#else
    sig = shift_right_sticky64((uint64_t)sig1 * sig2, 21);
    if (sig < 0x4000000)
        sig = sig << 1;
    else
        exp++;
    sig = round_even_grs_32(sig);
    if (sig > 0xffffff)
        exp++;
#endif
    if (exp < 1)
        return ((uint32_t)sign << 31);
    if (exp > 254)
#ifdef ROUND_TRUNC
        return (SF32_MAX | ((uint32_t)sign << 31));
#else
        return (SF32_INF | ((uint32_t)sign << 31));
#endif
    return sf32_pack(sign, exp, sig);
}

sfloat32 sf32_div(sfloat32 sf1, sfloat32 sf2)
{
    int exp1 = SF32_EXP(sf1);
    int exp2 = SF32_EXP(sf2);
    uint32_t sig1 = SF32_SIG(sf1);
    uint32_t sig2 = SF32_SIG(sf2);
    bool sign = SF32_SIGN(sf1) ^ SF32_SIGN(sf2);
    if (exp1 == 0xff) {
        if (sig1 || (exp2 == 0xff))
            return SF32_NAN;
        return (SF32_INF | ((uint32_t)sign << 31));
    }
    if (exp2 == 0xff) {
        if (sig2)
            return SF32_NAN;
        return ((uint32_t)sign << 31);
    }
    if (exp1 == 0) {
        if (exp2 == 0)
            return SF32_NAN;
        return ((uint32_t)sign << 31);
    }
    if (exp2 == 0)
        return (SF32_INF | ((uint32_t)sign << 31));
    int exp = exp1 - exp2 + 127;
    sig1 = sig1 | 0x800000;
    sig2 = sig2 | 0x800000;
    uint32_t sig;
#ifdef LONG_DIV
    uint64_t sig64;
    if (sig1 < sig2) {
        exp--;
        sig64 = (uint64_t)sig1 << 32;
    }
    else {
        sig64 = (uint64_t)sig1 << 31;
    }
#ifdef ROUND_TRUNC
    sig = ulong_div(sig64, sig2) >> 8;
#else
    uint32_t rem;
    sig = ulong_div_mod(sig64, sig2, &rem) >> 5;
    if (rem)
        sig |= 1;
    sig = round_even_grs_32(sig);
#endif
#else // !LONG_DIV
    if (sig1 < sig2) {
        exp--;
        sig1 <<= 8;
    }
    else {
        sig1 <<= 7;
    }
    sig2 <<= 8;
    sig = (uint64_t)sig1 * approx_recip(sig2) >> 31;
    uint64_t rem = (uint64_t)sig1 << 32;
    rem -= (uint64_t)sig * sig2;
    while (rem >= sig2) {
        sig++;
        rem -= sig2;
    }
#ifdef ROUND_TRUNC
    sig >>= 8;
#else
    sig >>= 5;
    if (rem)
        sig |= 1;
    sig = round_even_grs_32(sig);
#endif
#endif // LONG_DIV
    if (exp < 1)
        return ((uint32_t)sign << 31);
    if (exp > 254)
        return (SF32_INF | ((uint32_t)sign << 31));
    return sf32_pack(sign, exp, sig);
}

int32_t sf32_to_i32(sfloat32 sf)
{
    int exp = SF32_EXP(sf) - 127;
    if (exp < 0)
        return 0;
    bool sign = SF32_SIGN(sf);
    if (exp > 30)
        return (int32_t)0x80000000;
    uint32_t sig = SF32_SIG(sf) | 0x800000;
    int shift = 23 - exp;
    int32_t res;
    if (shift > 0)
        res = sig >> shift;
    else
        res = sig << (-shift);
    if (sign)
        res = -res;
    return res;
}

sfloat32 sf32_from_i32(int32_t i)
{
    if (i == 0)
        return 0;
    bool sign = i >> 31;
    if (sign)
        i = -i;
    int exp = 127 + 23;
#if 1
    int shift = count_leading_zero(i) - 8;
    if (shift > 0) {
        i <<= shift;
        exp -= shift;
    }
    else if (shift < 0) {
        shift = -shift;
        i >>= shift;
        exp += shift;
    }
#else
    while (i >= 0x1000000) {
        i = i >> 1;
        exp++;
    }
    while (i < 0x800000) {
        i = i << 1;
        exp--;
    }
#endif
    return sf32_pack(sign, exp, i);
}
