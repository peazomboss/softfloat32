#pragma once

#include <stdint.h>
#include <stdbool.h>

// 截断模式，可以减少代码体积，但是精度略低
// 需要则可以在include文件之前定义
// #define ROUND_TRUNC

// 使用长除法，默认不启用
// #define LONG_DIV

typedef uint32_t sfloat32;

// 将浮点数转换到软浮点数
sfloat32 sf32_from_float(float f);
// 将软浮点数转换到浮点数
float sf32_to_float(sfloat32 sf);

// 判断是否是NaN
bool sf32_isnan(sfloat32 sf);
// 判断是否无穷
bool sf32_isinf(sfloat32 sf);
// 获取符号位
bool sf32_sign(sfloat32 sf);

// 判断相等
bool sf32_eq(sfloat32 sf1, sfloat32 sf2);
// 判断不相等
bool sf32_ne(sfloat32 sf1, sfloat32 sf2);
// 判断小于
bool sf32_lt(sfloat32 sf1, sfloat32 sf2);
// 判断小于等于
bool sf32_le(sfloat32 sf1, sfloat32 sf2);
// 判断大于
bool sf32_gt(sfloat32 sf1, sfloat32 sf2);
// 判断大于等于
bool sf32_ge(sfloat32 sf1, sfloat32 sf2);

// 加法运算
sfloat32 sf32_add(sfloat32 sf1, sfloat32 sf2);
// 减法运算
sfloat32 sf32_sub(sfloat32 sf1, sfloat32 sf2);
// 乘法运算
sfloat32 sf32_mul(sfloat32 sf1, sfloat32 sf2);
// 除法运算
sfloat32 sf32_div(sfloat32 sf1, sfloat32 sf2);

// 转换到整数
int32_t sf32_to_i32(sfloat32 sf);
// 从整数转换
sfloat32 sf32_from_i32(int32_t i);
